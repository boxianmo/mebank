package model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.ServiceUtil;

class TransactionTest {

    private Transaction transaction = new Transaction();

    public void build() {
        transaction.setTransactionId("ACC001");
        transaction.setFromAccountId("ACC002");
        transaction.setToAccountId("ACC003");
        transaction.setCreateAt(ServiceUtil.stringToLocalDateTime("20/10/2018 12:47:55"));
        transaction.setAmount(25.00);
        transaction.setTransactionType(TransactionType.PAYMENT);
    }

    public void buildRevert() {
        transaction.setTransactionId("ACC001");
        transaction.setFromAccountId("ACC002");
        transaction.setToAccountId("ACC003");
        transaction.setCreateAt(ServiceUtil.stringToLocalDateTime("20/10/2018 12:47:55"));
        transaction.setAmount(25.00);
        transaction.setTransactionType(TransactionType.REVERSAL);
        transaction.setRelatedTransaction("ACC001");
    }

    @Test
    void testTransaction() {
        build();
        Assertions.assertEquals("ACC001", transaction.getTransactionId());
        Assertions.assertEquals("ACC002", transaction.getFromAccountId());
        Assertions.assertEquals("ACC003", transaction.getToAccountId());
        Assertions.assertEquals(ServiceUtil.stringToLocalDateTime("20/10/2018 12:47:55"), transaction.getCreateAt());
        Assertions.assertEquals( TransactionType.PAYMENT, transaction.getTransactionType());
    }

    @Test
    void testTransactionRevert() {
        buildRevert();
        Assertions.assertEquals("ACC001", transaction.getTransactionId());
        Assertions.assertEquals("ACC002", transaction.getFromAccountId());
        Assertions.assertEquals("ACC003", transaction.getToAccountId());
        Assertions.assertEquals(ServiceUtil.stringToLocalDateTime("20/10/2018 12:47:55"), transaction.getCreateAt());
        Assertions.assertEquals(TransactionType.REVERSAL, transaction.getTransactionType());
        Assertions.assertEquals("ACC001", transaction.getRelatedTransaction());
    }

}