package model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TransactionTypeTest {
    @Test
    void testPayment() {
        Assertions.assertEquals("PAYMENT", TransactionType.PAYMENT.getCode());
    }

    @Test
    void testReversal() {
        Assertions.assertEquals("REVERSAL", TransactionType.REVERSAL.getCode());
    }

}