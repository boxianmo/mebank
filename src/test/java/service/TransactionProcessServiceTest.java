package service;

import org.junit.jupiter.api.Test;
import utils.ServiceUtil;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TransactionProcessServiceTest {

    private TransactionProcessService transactionProcessService = new TransactionProcessService();
    private String csvPath = "src/test/resources/transaction.csv";

    @Test
    void testProcessorZero() {
        String acc = "AC001";
        LocalDateTime from = ServiceUtil.stringToLocalDateTime("20/10/2019 18:00:00");
        LocalDateTime to = ServiceUtil.stringToLocalDateTime("22/10/2019 09:30:00");
        String actual = transactionProcessService.processor(csvPath, acc, from ,to);
        assertEquals("Relative balance for the period is: $0.00\n" +
                "Number of transactions included is: 0", actual);
    }

    @Test
    void testProcessorOneSpend() {
        String acc = "ACC001";
        LocalDateTime from = ServiceUtil.stringToLocalDateTime("22/10/2019 18:00:00");
        LocalDateTime to = ServiceUtil.stringToLocalDateTime("22/10/2019 18:00:00");
        String actual = transactionProcessService.processor(csvPath, acc, from ,to);
        assertEquals("Relative balance for the period is: -$5.00\n" +
                "Number of transactions included is: 1", actual);
    }

    @Test
    void testProcessorReceiveOnly() {
        String acc = "ACC005";
        LocalDateTime from = ServiceUtil.stringToLocalDateTime("22/10/2019 21:30:00");
        LocalDateTime to = ServiceUtil.stringToLocalDateTime("22/10/2019 21:30:00");
        String actual = transactionProcessService.processor(csvPath, acc, from ,to);
        assertEquals("Relative balance for the period is: $5.00\n" +
                "Number of transactions included is: 1", actual);
    }

    @Test
    void testProcessorReceiveAndRevert() {
        String acc = "ACC002";
        LocalDateTime from = ServiceUtil.stringToLocalDateTime("22/10/2019 18:00:00");
        LocalDateTime to = ServiceUtil.stringToLocalDateTime("22/10/2019 18:00:00");
        String actual = transactionProcessService.processor(csvPath, acc, from ,to);
        assertEquals("Relative balance for the period is: $10.00\n" +
                "Number of transactions included is: 0", actual);
    }

    @Test
    void testProcessorSpendReceiveRevert() {
        String acc = "ACC334455";
        LocalDateTime from = ServiceUtil.stringToLocalDateTime("20/10/2018 12:00:00");
        LocalDateTime to = ServiceUtil.stringToLocalDateTime("20/10/2018 19:00:00");
        String actual = transactionProcessService.processor(csvPath, acc, from, to);
        assertEquals("Relative balance for the period is: -$14.50\n" +
                "Number of transactions included is: 0", actual);
    }
}