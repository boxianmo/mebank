package service;

import model.Transaction;
import org.junit.jupiter.api.Test;
import utils.ServiceUtil;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileInputServiceTest {
    @Test
    void testLoad() {
        FileInputService fileInputService = new FileInputService();
        String csvPath = "src/test/resources/transaction.csv";
        List<Transaction> load = fileInputService.load(csvPath);
        assertEquals(14, load.size());
        assertEquals("TX10001", load.get(0).getTransactionId());
        assertEquals("ACC334455", load.get(0).getFromAccountId());
        assertEquals("ACC778899", load.get(0).getToAccountId());
        assertEquals(ServiceUtil.stringToLocalDateTime("20/10/2018 12:47:55"), load.get(0).getCreateAt());
        assertEquals(25, load.get(0).getAmount());
        assertEquals("PAYMENT", load.get(0).getTransactionType().toString());
        assertEquals("TX10002", load.get(3).getRelatedTransaction());
    }

}