package service;

import model.Transaction;
import utils.ServiceUtil;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/***
 * Implement the core transaction process logic
 */
public class TransactionProcessService {

    private FileInputService fileInputService = new FileInputService();

    /***
     * Calculate transactionNumber and transactionTotal
     */
    public String processor(String path, String accountId, LocalDateTime from, LocalDateTime to) {

        List<Transaction> transactions = fileInputService.load(path);

        List<Transaction> spendList = getSpendList(transactions, accountId, from, to);
        List<Transaction> receiveList = getReceiveList(transactions, accountId, from, to);
        List<Transaction> revertList = getRevertList(transactions, accountId);

        Double spendTotal = getTotal(spendList);
        Double receiveTotal = getTotal(receiveList);
        Double revertTotal = getTotal(revertList);

        int transactionNumber = spendList.size() + receiveList.size() - revertList.size();
        double transactionTotal = receiveTotal + revertTotal - spendTotal;

        return messageBuilder(transactionNumber, transactionTotal);
    }

    /***
     * build the message for display
     */
    private String messageBuilder(int transactionNumber, double transactionTotal) {
        StringBuilder msg = new StringBuilder("Relative balance for the period is: ");
        if (transactionTotal < ServiceUtil.ZERO) {
            msg.append("-$");
        } else {
            msg.append("$");
        }
        msg.append(String.format("%.2f", Math.abs(transactionTotal)));
        msg.append("\n");
        msg.append("Number of transactions included is: ");
        msg.append(transactionNumber);

        return msg.toString();
    }

    /***
     * sum base on input list
     */
    private Double getTotal(List<Transaction> spendList) {
        return spendList.stream()
                .map(Transaction::getAmount)
                .reduce(ServiceUtil.ZERO, Double::sum);
    }

    /***
     * Filter: get the target transactions
     */
    private List<Transaction> getSpendList(List<Transaction> transactions, String accountId, LocalDateTime from, LocalDateTime to) {
        return transactions
                .stream()
                .filter(transaction -> transaction.getFromAccountId().equals(accountId))
                .filter(isEqualOrAfter(from))
                .filter(isBeforeOrEqual(to))
                .collect(Collectors.toList());
    }

    /***
     * Filter: get the target transactions
     */
    private List<Transaction> getReceiveList(List<Transaction> transactions, String accountId, LocalDateTime from, LocalDateTime to) {
        return transactions
                .stream()
                .filter(transaction -> transaction.getToAccountId().equals(accountId))
                .filter(isEqualOrAfter(from))
                .filter(isBeforeOrEqual(to))
                .collect(Collectors.toList());
    }

    /***
     * Filter: get the target transactions
     */
    private List<Transaction> getRevertList(List<Transaction> transactions, String accountId) {
        return transactions
                .stream()
                .filter(transaction -> transaction.getTransactionType().getCode().equals("REVERSAL"))
                .filter(transaction -> transaction.getFromAccountId().equals(accountId))
                .collect(Collectors.toList());
    }

    /**
     * Get transactions that are before or equal given local date time
     */
    private Predicate<Transaction> isBeforeOrEqual(LocalDateTime to) {
        return transaction -> (transaction.getCreateAt().isBefore(to) || transaction.getCreateAt().isEqual(to));
    }

    /**
     * Get transactions that are equal or after given local date time
     */
    private Predicate<Transaction> isEqualOrAfter(LocalDateTime from) {
        return transaction -> (transaction.getCreateAt().isAfter(from) || transaction.getCreateAt().isEqual(from));
    }
}
