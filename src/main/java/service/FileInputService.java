package service;

import model.Transaction;
import model.TransactionType;
import utils.ServiceUtil;

import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/***
 * Load the csv file and map to domain object
 */
public class FileInputService {
    private List<Transaction> transactions;

    public List<Transaction> load(String path) {

        try (InputStream is = new FileInputStream(new File(path))) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            br.readLine();
            transactions = br
                    .lines()
                    .map(mapToTransaction)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Load CSV fail, please check the input file");
            System.exit(0);
        }
        return transactions;
    }

    /***
     * map csv to domain object
     */
    private static Function<String, Transaction> mapToTransaction = (line) -> {
        String[] str = line.split("\t");
        Transaction transaction = new Transaction();
        transaction.setTransactionId(str[0]);
        transaction.setFromAccountId(str[1]);
        transaction.setToAccountId(str[2]);
        transaction.setCreateAt(ServiceUtil.stringToLocalDateTime(str[3]));
        transaction.setAmount(ServiceUtil.parseDouble(str[4]));
        transaction.setTransactionType(TransactionType.valueOf(str[5]));
        if (str.length > 6) {
            transaction.setRelatedTransaction(str[6]);
        }
        return transaction;
    };


}
