package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServiceUtil {
    public static final double ZERO = 0.0;

    /***
     * Covert String to LocalDateTime
     */
    public static LocalDateTime stringToLocalDateTime(String dateSting) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        return LocalDateTime.parse(dateSting, formatter);
    }

    /***
     * return 0.0 if input is null
     */
    public static double parseDouble(String s){
        if(s == null || s.isEmpty()) {
            System.out.println("empty");
            return ZERO;
        }
        else {
            return Double.parseDouble(s);
        }
    }
}
