package model;

import lombok.Getter;

@Getter
public enum TransactionType {
    PAYMENT("PAYMENT"), REVERSAL("REVERSAL");

    private String code;

    TransactionType(String code) {
        this.code = code;
    }
}
