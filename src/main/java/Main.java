import service.TransactionProcessService;
import utils.ServiceUtil;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Main {

    private TransactionProcessService transactionProcessService = new TransactionProcessService();

    public static void main(String[] args) {
        new Main().run(args);
    }

    private void run(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String path;
        if (args.length > 0) {
            path = args[0];
        } else {
            path = "src/main/resources/transaction.csv";
        }

        System.out.print("accountId:");
        String acc = scanner.nextLine().toUpperCase();
        System.out.print("from: ");
        LocalDateTime from = ServiceUtil.stringToLocalDateTime(scanner.nextLine());
        System.out.print("to: ");
        LocalDateTime to = ServiceUtil.stringToLocalDateTime(scanner.nextLine());
        System.out.println();

        String msg = transactionProcessService.processor(path, acc, from, to);
        System.out.println(msg);
    }
}

