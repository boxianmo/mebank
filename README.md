# ME bank code challenge

A system that analyses financial transaction records

## Getting Started
1. Clone this repository 
2. Build the project and run the Main.java
3. Csv path by default is transaction.csv in the project resources file. 
4. To use other csv file as input, **pass the csv path as an argument** when run the main file

## Prerequisites

```
Java version 8 or higher
Maven
```


## Design
1. The input csv is mapped to a list of domain Transaction object.
2. Apply filters to get target transactions as require by the use input.
3. Calculate the balance and transaction number.
4. Build the output message and display to the screen.


